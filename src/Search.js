import { useState } from 'react';
import './App.css';

function Search() {
  const [query, setQuery] = useState('');
  const [results, setResults] = useState([]);

  const handleSubmitQuery = () => {

    // write network request

  }

  return (
    <div className="App">
      <header className="App-header">
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-lg-4">
              <div className="mb-3">
                <label htmlFor="email" className="form-label">Query</label>
                <input
                  type="text"
                  className="form-control"
                  id="query"
                  value={ query }
                  onChange={ (e) => setQuery(e.value)}
                  placeholder="Enter query..."
                />
              </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm">
              <button
                type="button"
                className="btn btn-light"
                onClick={ handleSubmitQuery }
              >
                Submit
              </button>
            </div>
          </div>
      </header>
      <ol>
        {
          results.map((result) => {
            return <li> { result.name }</li>
          })
        }
      </ol>
    </div>
  );
}

export default Search;
