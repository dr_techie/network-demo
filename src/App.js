import { useState } from 'react';
import './App.css';

function App() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmitCredentials = async () => {

    console.log(email, password);
    // write network request

  }

  const getWhoAmI = () => {

    // get all my details

  }

  return (
    <div className="App">
      <header className="App-header">
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-lg-4">
              <div className="mb-3">
                <label htmlFor="email" className="form-label">Email address</label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  value={ email }
                  onChange={ (e) => setEmail(e.target.value)}
                  placeholder="Enter email..."
                />
              </div>
              <div className="mb-3">
                <label htmlFor="password" className="form-label">Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  placeholder="Enter password..."
                  onChange={ (e) => setPassword(e.target.value)}
                  value={ password }
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm">
              <button
                type="button"
                className="btn btn-light"
                onClick={ handleSubmitCredentials }
              >
                Submit
              </button>
            </div>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
